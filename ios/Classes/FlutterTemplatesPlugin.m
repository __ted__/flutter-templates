#import "FlutterTemplatesPlugin.h"
#import <flutter_templates/flutter_templates-Swift.h>

@implementation FlutterTemplatesPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterTemplatesPlugin registerWithRegistrar:registrar];
}
@end
