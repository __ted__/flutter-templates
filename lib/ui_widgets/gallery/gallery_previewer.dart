import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:photo_manager/photo_manager.dart';

class GalleryPreviewer extends StatefulWidget {
  final bool collapsed;
  final ScrollController controller;
  final ValueChanged<Uint8List> onTap;
  final Duration duration;
  final ScrollPhysics physics;
  const GalleryPreviewer(
      {@required this.collapsed, @required this.onTap, this.controller,this.physics,this.duration=Duration.zero});

  @override
  _GalleryPreviewerState createState() => _GalleryPreviewerState();
}

class _GalleryPreviewerState extends State<GalleryPreviewer> {
  List<AssetEntity> images = new List<AssetEntity>();

  @override
  void initState() {
    super.initState();
    _getGalleryImagePaths();
  }

  Future<void> _getGalleryImagePaths() async {
    List<AssetPathEntity> allImageTemp = await PhotoManager.getAssetPathList();

    for (var album in allImageTemp) {
      for (var image in await album.assetList) {
        if (!images.contains(image)) images.add(image);
      }
    }
    setState(() {
      // galleryImages = allImageTemp.reversed.toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
        height: widget.collapsed ? 0 : 400,
        duration: widget.duration,
        color: Colors.grey,
        child: GridView.builder(
          cacheExtent: 9,
          controller: widget.controller,
          physics: widget.physics ?? BouncingScrollPhysics(),
          itemCount: images.length + 1,
          padding: EdgeInsets.all(0),
          gridDelegate:
              new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
          itemBuilder: (BuildContext context, int i) {
            int index = i - 1;
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 1)),
              child: i == 0
                  ? InkWell(
                      child: Container(
                        color: Colors.black12,
                        child: Icon(
                          Icons.photo_camera,
                          color: Colors.white54,
                          size: 50,
                        ),
                      ),
                      onTap: () async {
                        File file = await ImagePicker.pickImage(
                            source: ImageSource.camera);
                        if (file != null) {
                          Uint8List data = await file.readAsBytes();
                          widget.onTap(data);
                        }
                      },
                    )
                  : InkWell(
                      onTap: () async {
                        Uint8List data = await images[index].fullData;
                        widget.onTap(data);
                      },
                      // onLongPress: () async {
                      //   // File file = File(galleryImages[index].);
                      //   setLoading = true;

                      //   // var data = await file.readAsBytes();
                      //   Uint8List data = await galleryImages[index].fullData;
                      //   _uploadFile(data);
                      // },
                      child: FutureBuilder(
                        future: images[index].thumbDataWithSize(200, 200),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData) {
                            return Image.memory(
                              snapshot.data,
                              fit: BoxFit
                                  .cover, //just for testing, will fill with image later
                            );
                          }
                          return Container(
                            color: Colors.grey,
                          );
                        },
                      )),
            );
          },
        ));
  }
}
