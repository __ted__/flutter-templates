import 'package:flutter/material.dart';
import 'package:flutter_templates/ui_widgets/navigation/bottom_navigation.dart';
import 'package:flutter_templates/ui_widgets/navigation/navigation_body.dart';

class MyBackgroundContainer extends StatelessWidget {
  final Widget child;
  final EdgeInsets padding;

  final String background;

  final double height;

  // final bool isOverTab;

  const MyBackgroundContainer({
    Key key,
    this.child,
    this.padding = EdgeInsets.zero,
    this.background,
    this.height = -1,
    // this.isOverTab=false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double bottomHeight = 0;
    try {
      if (NavigationBody.of(context) != null) {
        var keyContext = BottomNavigation.bottomNavigationKey.currentContext;
        RenderBox box = keyContext.findRenderObject() as RenderBox;
        bottomHeight = box.size.height;
      }
    } catch (e) {
      print(e);
    }
    double minheight =
        MediaQuery.of(context).size.height - bottomHeight; //TODO PLEASE
    double maxHeight = double.infinity;
    if (height != null && height > 0) {
      maxHeight = height;
    }
    return Container(
      constraints: BoxConstraints(minHeight: minheight, maxHeight: maxHeight),
      decoration: this.background == null
          ? null
          : BoxDecoration(
              image: DecorationImage(
                  repeat: ImageRepeat.repeat,
                  alignment: Alignment.topCenter,
                  // fit: BoxFit.fitHeight,
                  image: AssetImage(this.background))),
      padding: this.padding,
      child: this.child,
    );
  }
}
