import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final VoidCallback onPressed;

  final String text;
  final TextStyle textStyle;
  final Color backgroundColor;

  const MyButton(this.text,
      {Key key, this.onPressed, this.backgroundColor, this.textStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: 60,
      minWidth: 250,
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      onPressed: onPressed,
      child: Padding(
        padding: const EdgeInsets.only(top: 2.5),
        child: Text(
          text,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
            color: Colors.white,
          ).merge(textStyle),
        ),
      ),
      disabledColor: Colors.grey.withOpacity(0.3),
      disabledTextColor: Colors.black12,
      color: backgroundColor,
    );
  }
}
