import 'package:flutter/material.dart';

class MyCard extends StatelessWidget {
  final Widget child;
  final Color color;

  final EdgeInsets padding;
  final bool outline;

  const MyCard(
      {Key key,
      this.child,
      this.padding,
      this.color = Colors.white,
      this.outline = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.symmetric(vertical: 4),
        shape: RoundedRectangleBorder(
            side: BorderSide(
                color: Colors.red,
                width: 1,
                style: outline ? BorderStyle.solid : BorderStyle.none),
            borderRadius: BorderRadius.circular(6)),
        color: color,
        elevation: 3,
        child: Padding(
          padding: padding ?? const EdgeInsets.all(6.0),
          child: this.child,
        ));
  }
}
