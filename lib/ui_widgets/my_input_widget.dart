import 'package:flutter/material.dart';
import 'package:flutter_templates/ui_widgets/my_card_widget.dart';

class MyInput extends FormField<String> {
  final String hintText;

  final ValueChanged<String> onChanged;
  final TextInputType keyboardType;

  final TextAlign textAlignment;
  final bool readOnly;
  final TextEditingController controller;
  final int maxLength;
  final BoxConstraints inputConstraints;

  final FocusNode focusNode;

  final VoidCallback onTap;
  final Widget prefix;
  final Widget sufix;

  final EdgeInsets padding;
  final EdgeInsets margin;
  final Color color;

  final Key key;

  MyInput(
      {this.key,
      this.controller,
      this.hintText = '',
      this.textAlignment = TextAlign.start,
      this.keyboardType = TextInputType.text,
      this.readOnly = false,
      this.prefix,
      this.sufix,
      this.padding,
      this.margin,
      this.maxLength,
      this.inputConstraints,
      this.onChanged,
      this.onTap,
      this.focusNode,
      this.color,
      FormFieldSetter<String> onSaved,
      FormFieldValidator<String> validator,
      String initialValue = "",
      bool autovalidate = false})
      : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidate: autovalidate,
            builder: (FormFieldState<String> state) {
              return MyCard(
                color: color,
                child: Container(
                  padding:
                      padding ?? const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                          constraints: inputConstraints,
                          child: Row(
                            children: <Widget>[
                              prefix ??
                                  Container(
                                    width: 0,
                                    height: 0,
                                  ),
                              Expanded(
                                  child: TextFormField(
                                focusNode: focusNode,
                                controller:
                                    initialValue == null ? controller : null,
                                enableInteractiveSelection: !readOnly,
                                readOnly: readOnly,
                                maxLines:
                                    keyboardType == TextInputType.multiline
                                        ? null
                                        : 1,
                                keyboardType: keyboardType,
                                textAlign: textAlignment,
                                initialValue:
                                    initialValue == null ? null : state.value,
                                onChanged: (text) {
                                  state.didChange(text);
                                  if (onChanged != null) {
                                    onChanged(text);
                                  }
                                  controller.text = text;
                                },
                                maxLength: maxLength,
                                decoration: InputDecoration(
                                  hintText: hintText,
                                  hintStyle: TextStyle(color: Colors.black38),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.transparent),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.transparent),
                                  ),
                                  // prefixIcon: prefix,
                                ),
                              )),
                              sufix ??
                                  Container(
                                    width: 0,
                                    height: 0,
                                  ),
                            ],
                          )),
                      // Expanded(child:Container(child:Text(""))),
                      // Spacer(),
                      Container(
                        alignment: Alignment.centerLeft,
                        // duration: Duration(milliseconds: 300),
                        child: state.hasError
                            ? Text(state.errorText,
                                style: TextStyle(color: Colors.red))
                            : null,
                      )
                    ],
                  ),
                ),
              );
            });

  // MyInputState createState() => MyInputState();
}

// class MyInputState extends State<MyInput> {
//   @override
//   void initState() {
//     super.initState();
//     //  _validated = validator(widget.controller.text);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MyCard(
//       color: widget.color,
//       validated: _validated,
//       validatedText: widget.validateText,
//       child: Container(
//         // margin: margin ?? EdgeInsets.zero,
//         padding: widget.padding ?? const EdgeInsets.symmetric(horizontal: 10.0),
//         child: Row(
//           children: <Widget>[
//             widget.prefix ??
//                 Container(
//                   width: 0,
//                   height: 0,
//                 ),
//             Expanded(
//               child: GestureDetector(
//                 onTap: widget.onTap,
//                 child: AbsorbPointer(
//                   absorbing: widget.readOnly,
//                   child: TextFormField(
//                     focusNode: widget.focusNode,
//                     controller: widget.controller,
//                     enableInteractiveSelection: !widget.readOnly,
//                     readOnly: widget.readOnly,
//                     maxLines: widget.keyboardType == TextInputType.multiline
//                         ? null
//                         : 1,
//                     keyboardType: widget.keyboardType,
//                     textAlign: widget.textAlignment,
//                     onChanged: (text) {
//                       setState(() {
//                         _validated = widget.validator(text);
//                       });
//                     },
//                     maxLength: widget.maxLength,
//                     decoration: InputDecoration(
//                       hintText: widget.hintText,
//                       hintStyle: TextStyle(color: Colors.black38),
//                       enabledBorder: UnderlineInputBorder(
//                         borderSide: BorderSide(color: Colors.transparent),
//                       ),
//                       focusedBorder: UnderlineInputBorder(
//                         borderSide: BorderSide(color: Colors.transparent),
//                       ),
//                       // prefixIcon: prefix,
//                     ),
//                     // onEditingComplete: () {
//                     //   print("ola");
//                     //   onChanged(controller.text);
//                     // },
//                     // onSaved: (a) {
//                     //   print(a);
//                     //   onChanged(a);
//                     // },
//                     // onFieldSubmitted: (a) {
//                     //   print(a);
//                     //   onChanged(a);
//                     // },
//                   ),
//                 ),
//               ),
//             ),
//             widget.sufix ??
//                 Container(
//                   width: 0,
//                   height: 0,
//                 ),
//           ],
//         ),
//       ),
//     );
//   }
// }
