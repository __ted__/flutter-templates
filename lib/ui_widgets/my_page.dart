import 'package:flutter/material.dart';

import 'my_background_container.dart';

class MyPage extends StatelessWidget {
  final EdgeInsets padding;
  final ScrollPhysics physics;
  final Widget child;
  final List<Widget> actions; //todo change
  final VoidCallback onBackPressed;
  final double height;
  final String background;

  MyPage(
      {this.padding = EdgeInsets.zero,
      this.physics,
      this.child,
      this.actions,
      this.onBackPressed,
      this.background,
      this.height});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
            padding: padding,
            physics: physics ?? ClampingScrollPhysics(),
            child: MyBackgroundContainer(
              child: child,
              height: height,
              background: background,
            )),
        Container(
            // padding: EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
                // backgroundBlendMode: BlendMode.srcOut,
                gradient: LinearGradient(
                    colors: [Colors.black26, Colors.transparent],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0, 1])),
            child: SafeArea(
              child: Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Navigator.of(context).canPop()
                          ?
                          // Hero(
                          //     tag: "back_button",
                          // child:
                          RawMaterialButton(
                              constraints:
                                  BoxConstraints(maxWidth: 60, maxHeight: 60),
                              onPressed: () {
                                if (onBackPressed != null) {
                                  onBackPressed();
                                } else
                                  Navigator.of(context).pop();
                              },
                              child: new Icon(
                                Icons.arrow_back,
                                color: Colors.grey,
                                // size: 20.0,
                              ),
                              shape: new CircleBorder(),
                              elevation: 2.0,
                              fillColor: Colors.white,
                              padding: const EdgeInsets.all(10.0),
                              // )
                            )
                          : SizedBox(
                              height: 0,
                            ),
                      actions != null
                          ? RawMaterialButton(
                              constraints:
                                  BoxConstraints(maxWidth: 60, maxHeight: 60),
                              onPressed: () {},
                              child: new Icon(
                                Icons.more_vert,
                                color: Colors.grey,
                                // size: 25.0,
                              ),
                              shape: new CircleBorder(),
                              elevation: 2.0,
                              fillColor: Colors.white,
                              padding: const EdgeInsets.all(10.0),
                            )
                          : SizedBox(
                              height: 0,
                            )
                    ],
                  )),
            )),
      ],
    );
  }
}
