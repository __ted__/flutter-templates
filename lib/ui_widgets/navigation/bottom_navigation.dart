import 'package:flutter/material.dart';
import 'package:flutter_templates/ui_widgets/navigation/bottom_navigation_item.dart';

class BottomNavigation extends StatelessWidget {
  BottomNavigation({this.currentTab, this.onSelectTab, this.tabs});
  final MyBottomNavigationItem currentTab;
  final List<MyBottomNavigationItem> tabs;
  final ValueChanged<int> onSelectTab;
  static GlobalKey bottomNavigationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      key: bottomNavigationKey,
      // height: 70,// bottom navigation bar height
      child: BottomNavigationBar(
        elevation: 20,
        iconSize: 30,
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: tabs.map<BottomNavigationBarItem>((f){
          return f.buildItem();
        }).toList(),
        currentIndex: tabs.indexOf(currentTab),
        onTap: (index) => onSelectTab(index),
        // showSelectedLabels: false,
        // showUnselectedLabels: false,
        selectedFontSize: 10,
        unselectedFontSize: 10,
      ),
    );
  }

  // BottomNavigationBarItem _buildItem({TabModel tabItem}) {
  //   bool isActive = tabItem == currentTab;
  //   Color backColor = isActive ? Colors.orange : Colors.transparent;
  //   Color iconColor = isActive ? Colors.white : Colors.grey;
  //   return BottomNavigationBarItem(
  //     icon: Container(
  //       decoration: BoxDecoration(
  //           color: backColor,
  //           borderRadius: BorderRadius.all(Radius.elliptical(20, 18))),
  //       padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
  //       child: Image.asset(
  //         tabItem.icon,
  //         scale: 3,
  //         color: iconColor,
  //         fit: BoxFit.fitWidth,
  //       ),
  //     ),
  //     title: Padding(
  //       padding: const EdgeInsets.only(top: 3.0,left: 4,right: 4),
  //       child: Text(
  //         tabItem.name.toUpperCase(),
  //         style: TextStyle(
  //             color: Colors.grey,
  //             // fontFamily: AppLocalizations.instance.translate("font_family"),
  //             fontWeight: FontWeight.bold),
  //       ),
  //     ),
  //   );
  // }
}
