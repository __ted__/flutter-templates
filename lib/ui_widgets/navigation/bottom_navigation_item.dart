import 'package:flutter/material.dart';
import 'package:flutter_templates/ui_widgets/navigation/tabmodel.dart';

abstract class MyBottomNavigationItem{
  TabModel model;
  MyBottomNavigationItem({this.model});
  BottomNavigationBarItem buildItem();
}