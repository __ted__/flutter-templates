import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_templates/ui_widgets/navigation/bottom_navigation_item.dart';

import 'bottom_navigation.dart';

class NavigationBody extends StatefulWidget {
  final List<MyBottomNavigationItem> pages;

  NavigationBody({this.pages});
  @override
  _NavigationBodyState createState() => _NavigationBodyState();
  static void selectTab(BuildContext context, int index) {
    _NavigationBodyState state =
        context.ancestorStateOfType(const TypeMatcher<_NavigationBodyState>());
    state._selectTab(index);
  }

   static State of(BuildContext context) {
    return context.ancestorStateOfType(const TypeMatcher<_NavigationBodyState>());
  }
}

class _NavigationBodyState extends State<NavigationBody> {
  Queue<int> tabQueue = new Queue<int>();
  MyBottomNavigationItem get currentTab => widget.pages[tabQueue.last];

  @override
  void initState() {
    super.initState();
    tabQueue.add(0);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          bool canpop =
              await currentTab.model.navigator.currentState.maybePop();
          if (!canpop) {
            _removeTab();
            canpop = tabQueue.length > 0;
          }
          return !canpop;
        },
        child: Scaffold(
            body: Stack(
                children: widget.pages.map<Widget>((tab) {
              return _buildOffstageNavigator(tab);
            }).toList()),
            bottomNavigationBar: BottomNavigation(
              currentTab: currentTab,
              tabs: widget.pages,
              onSelectTab: _selectTab,
            )));
  }

  Widget _buildOffstageNavigator(MyBottomNavigationItem tab) {
    return Offstage(
        offstage: currentTab != tab,
        child: Navigator(
            key: tab.model.navigator,
            // initialRoute: TabNavigatorRoutes.root,
            onGenerateRoute: (routeSettings) {
              return MaterialPageRoute(builder: (context) => tab.model.page);
            }));
  }

  void _selectTab(int tabItem) {
    setState(() {
      if (currentTab == widget.pages[tabItem]) {
        // while (widget.pages[tabItem].navigator.currentState.canPop()) {
        //   widget.pages[tabItem].navigator.currentState.pop();
        // }
        widget.pages[tabItem].model.navigator.currentState.popUntil((x) {
          return x.isFirst;
        });
      } else
        tabQueue.add(tabItem);
    });
  }

  void _removeTab() {
    if (tabQueue.length > 1) {
      setState(() {
        tabQueue.removeLast();
      });
    }
  }
}
