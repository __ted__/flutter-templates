import 'package:flutter/material.dart';

import '../page.dart';

class TabModel
{
  Page page;
  String name;
  IconData icon;
  GlobalKey<NavigatorState> navigator;

  TabModel({@required this.page, @required this.icon, @required this.name}){
    this.navigator = GlobalKey<NavigatorState>();
  }
  
}