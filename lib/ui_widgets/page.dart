import 'package:flutter/widgets.dart';

abstract class Page extends StatefulWidget {
  const Page({Key key}) : super(key: key);
}