import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SMSCodeField extends StatefulWidget {
  final int length;
  final ValueChanged<String> onPinChange;

  SMSCodeField({Key key, this.length = 3, this.onPinChange}) : super(key: key);

  _SMSCodeFieldState createState() => _SMSCodeFieldState();
}

class _SMSCodeFieldState extends State<SMSCodeField> {
  List<FocusNode> focuses;
  List<TextEditingController> controllers;

  @override
  void initState() {
    super.initState();
    focuses = new List<FocusNode>();
    controllers = new List<TextEditingController>();
    for (var i = 0; i < widget.length; i++) {
      focuses.add(new FocusNode());
      controllers.add(new TextEditingController());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(widget.length, (i) {
          return Container(
            margin: EdgeInsets.all(2),
            padding: EdgeInsets.only(top:2.5),
            width: 50,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                border: new Border.all(color: Colors.black26, width: 2)),
            child: TextField(
              textAlign: TextAlign.center,
              maxLength: 1,
              style: TextStyle(
                  fontSize: 28,
                  fontFamily: 'Walkers',
                  fontWeight: FontWeight.bold),
              decoration: InputDecoration(
                counterText: "",
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent),
                ),
              ),
              onChanged: (a) {
                if (a.isEmpty) {
                  FocusScope.of(context).requestFocus(focuses[i - 1]);
                } else if (i < widget.length - 1) {
                  FocusScope.of(context).requestFocus(focuses[i + 1]);
                  controllers[i + 1].text = "";
                }

                String pin = '';
                for (var item in controllers) {
                  pin += item.text;
                }
                widget.onPinChange(pin);
              },
              keyboardType: TextInputType.number,

              focusNode: focuses[i],
              controller: controllers[i],
              // mov
            ),
          );
        }));
  }
}
