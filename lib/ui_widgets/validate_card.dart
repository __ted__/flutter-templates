import 'package:flutter/material.dart';
import 'package:flutter_templates/ui_widgets/my_card_widget.dart';

// typedef Validate<T> = bool Function(T value);

class ValidateCard<T> extends FormField<T> {
  // final Color color;
  // final Widget child;
  ValidateCard(
      {Color color,
      Widget child,
      FormFieldSetter<T> onSaved,
      FormFieldValidator<T> validator,
      T initialValue,
      bool autovalidate = false})
      : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidate: autovalidate,
            builder: (FormFieldState<T> state) {
              return MyCard(
                color: color,
                outline: state.hasError,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    child,
                    AnimatedContainer(
                      padding: const EdgeInsets.only(left: 8.0, top: 0.0),
                      alignment: Alignment.centerLeft,
                      duration: Duration(milliseconds: 300),
                      child: state.hasError
                          ? Text(state.errorText,
                              style: TextStyle(color: Colors.red))
                          : null,
                    )
                  ],
                ),
              );
            });

  ValidateCard.build(
      {FormFieldBuilder<T> builder,
      FormFieldSetter<T> onSaved,
      FormFieldValidator<T> validator,
      Alignment textAlignment = Alignment.centerLeft,
      T initialValue,
      bool autovalidate = false})
      : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidate: autovalidate,
            builder: (state) {
              return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    builder(state),
                    AnimatedContainer(
                      padding: const EdgeInsets.only(left: 8.0, top: 0.0),
                      alignment: textAlignment,
                      duration: Duration(milliseconds: 300),
                      child: state.hasError
                          ? Text(state.errorText,
                              style: TextStyle(color: Colors.red))
                          : null,
                    )
                  ]);
            });
}
